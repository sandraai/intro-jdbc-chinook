package com.scania.saib5g.introjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntroJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntroJdbcApplication.class, args);
    }

}
