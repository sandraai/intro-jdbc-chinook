package com.scania.saib5g.introjdbc.models;

public class CustomerExpenditureDto {
    private int customerId;
    private double expenditure;


    public CustomerExpenditureDto(int customerId, double expenditure) {
        this.customerId = customerId;
        this.expenditure = expenditure;
    }


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public double getExpenditure() {
        return expenditure;
    }

    public void setExpenditure(double expenditure) {
        this.expenditure = expenditure;
    }
}
