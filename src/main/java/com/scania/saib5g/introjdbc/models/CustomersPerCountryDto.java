package com.scania.saib5g.introjdbc.models;

public class CustomersPerCountryDto {
    private String country;
    private int numberOfCustomers;


    public CustomersPerCountryDto(String country, int numberOfCustomers) {
        this.country = country;
        this.numberOfCustomers = numberOfCustomers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }
}
