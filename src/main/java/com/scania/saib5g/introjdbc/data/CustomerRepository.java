package com.scania.saib5g.introjdbc.data;

import com.scania.saib5g.introjdbc.models.Customer;
import com.scania.saib5g.introjdbc.models.CustomerExpenditureDto;
import com.scania.saib5g.introjdbc.models.CustomersPerCountryDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@Service
public class CustomerRepository {
    @Value("${spring.datasource.url}")
    private String url;

    public List<Customer> getAllCustomers () {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer";
        List<Customer> customers = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    customers.add(new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"),
                            resultSet.getString("last_name"), resultSet.getString("country"),
                            resultSet.getString("postal_code"), resultSet.getString("phone"),
                            resultSet.getString("email")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Customer getCustomerById(int id) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE customer_id = ?";
        Customer customer = null;
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    customer = new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"),
                            resultSet.getString("last_name"), resultSet.getString("country"),
                            resultSet.getString("postal_code"), resultSet.getString("phone"),
                            resultSet.getString("email"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public Customer getCustomerByName(String name) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE first_name LIKE ?";
        Customer customer = null;
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    customer = new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"),
                            resultSet.getString("last_name"), resultSet.getString("country"),
                            resultSet.getString("postal_code"), resultSet.getString("phone"),
                            resultSet.getString("email"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public List<Customer> getCustomersSubset(int startNo, int endNo) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();
        int offset = startNo -1;
        int limit = endNo - offset;
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    customers.add(new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"),
                            resultSet.getString("last_name"), resultSet.getString("country"),
                            resultSet.getString("postal_code"), resultSet.getString("phone"),
                            resultSet.getString("email")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Customer addCustomer(Customer customer) {
        String sql = "INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());
            System.out.println(preparedStatement.executeUpdate());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getCustomerById(customer.getCustomerId());
    }
    public Customer modifyCustomer(Customer customer) {
        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, customer.getCustomerId());
            System.out.println(preparedStatement.executeUpdate());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getCustomerById(customer.getCustomerId());
    }

    public List<CustomersPerCountryDto> getCustomersPerCountry() {
        String sql = "SELECT country, COUNT (*) as number_of_customers FROM customer GROUP BY country ORDER BY number_of_customers DESC";
        List<CustomersPerCountryDto> customersPerCountryList = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    customersPerCountryList.add(new CustomersPerCountryDto(resultSet.getString("country"), resultSet.getInt("number_of_customers")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customersPerCountryList;
    }

    public List<CustomerExpenditureDto> getCustomersExpenditure() {
        String sql = "SELECT customer_id, SUM(invoice.total) as total_amount FROM invoice GROUP BY customer_id ORDER BY total_amount DESC";
        List<CustomerExpenditureDto> customerExpenditureList = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    customerExpenditureList.add(new CustomerExpenditureDto(resultSet.getInt("customer_id"), resultSet.getInt("total_amount")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerExpenditureList;
    }
}
