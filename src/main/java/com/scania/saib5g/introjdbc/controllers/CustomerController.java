package com.scania.saib5g.introjdbc.controllers;

import com.scania.saib5g.introjdbc.data.CustomerRepository;
import com.scania.saib5g.introjdbc.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping
    public ResponseEntity<List> getCustomers () {

        return new ResponseEntity<>(customerRepository.getAllCustomers(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomerById (@PathVariable int id) {

        return new ResponseEntity<>(customerRepository.getCustomerById(id), HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<Customer> getCustomerByName (@PathVariable String name) {

        return new ResponseEntity<>(customerRepository.getCustomerByName(name), HttpStatus.OK);
    }

    @GetMapping("/subset/{startNo}/{endNo}")
    public ResponseEntity<List> getCustomersSubset (@PathVariable int startNo, @PathVariable int endNo) {

        return new ResponseEntity<>(customerRepository.getCustomersSubset(startNo, endNo), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Customer> addCustomer (@RequestBody Customer customer) {
        return new ResponseEntity<>(customerRepository.addCustomer(customer), HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity<Customer> modifyCustomer (@RequestBody Customer customer) {
        return new ResponseEntity<>(customerRepository.modifyCustomer(customer), HttpStatus.OK);
    }

    @GetMapping("/percountry")
    public ResponseEntity<List> getCustomersPerCountry () {

        return new ResponseEntity<>(customerRepository.getCustomersPerCountry(), HttpStatus.OK);
    }

    @GetMapping("/expenditure")
    public ResponseEntity<List> getCustomersExpenditure () {

        return new ResponseEntity<>(customerRepository.getCustomersExpenditure(), HttpStatus.OK);
    }

}
